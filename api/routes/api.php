<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BrandsController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\GamesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['App\Http\Middleware\JsonMiddleware'])->group(function () {
    Route::get('/brands', [BrandsController::class, 'index']);
    Route::get('/countries', [CountriesController::class, 'index']);
    Route::get('/games', [GamesController::class, 'index']);
});

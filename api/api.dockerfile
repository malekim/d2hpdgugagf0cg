FROM php:7.4.0-fpm

RUN mkdir -p api

WORKDIR /api/

COPY /api/ .

RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq \ 
    git \
    curl \
    patch \
    unzip \
    libc-dev \
    libgd-dev \
    libbz2-dev \
    libldb-dev \
    libpng-dev \
    libzip-dev \
    libgmp-dev \
    pkg-config \
    libwebp-dev \
    libjpeg-dev \
    libldap2-dev \
    libmcrypt-dev \
    libfreetype6-dev

RUN docker-php-ext-configure gd \
    --with-webp=/usr/include/ \
    --with-jpeg=/usr/include/ \
    --with-freetype=/usr/include/

RUN apt-get clean; docker-php-ext-install gd gmp bcmath mysqli pdo_mysql zip pcntl ldap opcache exif

RUN apt-get update && apt-get install -y libmcrypt-dev && \
    pecl install redis && \
    docker-php-ext-enable redis && \
    pecl install mcrypt-1.0.3 && \
    docker-php-ext-enable mcrypt

RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
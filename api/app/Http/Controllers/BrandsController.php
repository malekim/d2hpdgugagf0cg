<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandsController extends Controller
{
    /**
     * Get list of brands
     * 
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {
        return Brand::get();
    }
}

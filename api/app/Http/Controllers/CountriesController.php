<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;

class CountriesController extends Controller
{
    /**
     * Get list of countries
     * 
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {
        return Country::get();
    }
}

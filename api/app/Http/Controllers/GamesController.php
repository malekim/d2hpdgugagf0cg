<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;

class GamesController extends Controller
{
    /**
     * Get list of games
     * 
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request) {
        return Game::where(['active' => 1])->with('brandBlocks', 'brandGames', 'countryBlocks')->get();
    }
}

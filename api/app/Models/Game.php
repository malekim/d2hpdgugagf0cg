<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BrandBlock;
use App\Models\CountryBlock;

class Game extends Model
{
    use HasFactory;
    protected $table = 'game';

    public function brandBlocks() {
        return $this->hasMany(BrandBlock::class, "launchcode", "launchcode");
    }

    public function brandGames() {
      return $this->hasMany(BrandGame::class, "launchcode", "launchcode");
  }

    public function countryBlocks() {
        return $this->hasMany(CountryBlock::class, "launchcode", "launchcode");
    }
}

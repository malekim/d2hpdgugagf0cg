<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryBlock extends Model
{
    use HasFactory;
    protected $table = 'game_country_block';
}

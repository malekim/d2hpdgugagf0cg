<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrandBlock extends Model
{
    use HasFactory;
    protected $table = 'game_brand_block';
}

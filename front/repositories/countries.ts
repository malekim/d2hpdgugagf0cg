class CountriesRepository {
  axios: any
  api_url: string

  constructor(axios: any) {
    this.axios = axios
    this.api_url = process.env.API_URL || ''
  }

  async index() {
    return this.axios.get(`${this.api_url}countries`)
  }
}

export default CountriesRepository
